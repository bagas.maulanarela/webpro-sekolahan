<?php

use Illuminate\Database\Seeder;
use Illuminate\Foundation\Auth\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
        'name' => 'admin1',
        'email' => 'admin1',
        'password' => bcrypt('qwe123')
        ]);
    }
}
